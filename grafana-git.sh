#!/bin/bash

set -e
version=$(grafana-server -v | cut -d ' ' -f 2)
if [ "$(which grafana-server)" == "" ]; then
    echo "grafana is NOT installed! exiting..."
    exit
fi
path=~/grafana-git
declare -A File_Locations
#File_Locations[package.json]=$path/grafana-$version/package.json
File_Locations[doctec]=$path/grafana-$version/public/img/
File_Locations[.github]=$path/grafana-$version
#File_Locations[.yarn]=$path/grafana-$version
#File_Locations[.yarnrc.yml]=$path/grafana-$version


if [ -f "$path/zips/v$version.zip" ]; 
then
    if [ -d "$path/grafana-$version" ]; then
        echo "version already installed"
    else 
        echo "version already downloaded"
        unzip -q -o $path/zips/v$version.zip -d $path
        echo "files extracted"
    fi
else
    wget https://github.com/grafana/grafana/archive/refs/tags/v$version.zip -P $path/zips
    echo "version downloaded"
    unzip -q $path/zips/v$version.zip -d $path
    echo "files extracted"
fi
cd $path/customfiles
git pull https://gitlab.com/oInspector/customfiles.git
for f in *; do
  echo "replacing $f"
  find $path/grafana-$version -name "$f" -exec cp $path/customfiles/$f {} \;

done
cd $path/customfiles/customLocations/

for key in "${!File_Locations[@]}"; do
    echo "$key ${File_Locations[$key]}"
    cp -r $key ${File_Locations[$key]}
done

#builing
cd $path/grafana-$version/
yarn install --immutable
yarn themes:generate && yarn dev

cd $path/grafana-$version/
mkdir artifacts
cp public/views artifacts -r
cp public/sass artifacts -r
cp public/build artifacts -r
tar -czvf artifacts.tar.gz artifacts

sudo systemctl stop grafana-server.service
sudo cp -r $path/grafana-$version/public /usr/share/grafana/
sudo systemctl start grafana-server.service
#python3 -c 'import yaml;f=open("$path/grafana-$version/.github/workflows/build.yml");y=yaml.safe_load(f);y["on"]["push"]["branches"] = "$version";'

#echo "input git username"
#read username
#echo "input git password"
#read password
#username=FlexFreak
#password=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

#cd $path/grafana-$version
#git init
#git branch -m $version
#git add .
#git commit -m init
#git remote add origin https://github.com/$username/grafana-git
#git remote set-url origin https://$username:$password@github.com/$username/grafana-git
#git push --set-upstream origin $version
